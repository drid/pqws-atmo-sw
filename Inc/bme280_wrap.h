#include <bme280.h>
#include <cmsis_os.h>
#include "stm32l4xx_hal.h"

extern I2C_HandleTypeDef hi2c1;

int8_t bme280_atmo_init();
int8_t bme280_read_sensor_data_normal_mode();

