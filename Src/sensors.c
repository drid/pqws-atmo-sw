/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "sensors.h"
#include <cmsis_os.h>

extern TIM_HandleTypeDef htim2, htim1;
extern osTimerId preheatTimerHandle;

void sensor_power_off() {
	fan_disable();
	sensor_MQ7_power(POWER_OFF);
	sensor_MQ131_power(POWER_OFF);
	sensor_MICS_power(POWER_OFF);
}

/**
 * Read raw adc channel value
 * @param adc
 * @param channel
 * @return
 */
uint16_t sensor_get_raw(ADC_HandleTypeDef *adc, uint32_t channel) {
	uint16_t value=0;
	ADC_ChannelConfTypeDef sConfig;
	sConfig.Channel = channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(adc, &sConfig) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}
	if (HAL_ADCEx_Calibration_Start(adc, ADC_SINGLE_ENDED) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_ADC_Start(adc) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_ADC_PollForConversion(adc, 10) != HAL_OK) {
		Error_Handler();
	} else {
		value = HAL_ADC_GetValue(adc);
	}
	return value;
}

/**
 * Set PWM duty cycle
 * @param htimer
 * @param channel
 * @param pulse
 */
void set_pwm_duty_cycle(TIM_HandleTypeDef *htimer, uint32_t channel, uint32_t pulse) {
	TIM_OC_InitTypeDef sConfigOC;
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = pulse;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	sConfigOC.OCNPolarity = TIM_OCNPOLARITY_LOW;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	sConfigOC.OCIdleState = TIM_OCIDLESTATE_SET;
	sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;

	if (HAL_TIM_PWM_ConfigChannel(htimer, &sConfigOC, channel) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}
}

/**
 * Enable fan
 * @param power 0-100 % power setting
 */
void fan_power(uint8_t power) {
	if (power > 0) {
		set_pwm_duty_cycle(&htim2, TIM_CHANNEL_1, power * 20);
		HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	} else {
		fan_disable();
	}
}

/**
 * Stop FAN
 */
void fan_disable() {
	HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
}

/**
 * Run a FAN test from 100% to 20%
 */
void fan_test() {
	for (uint8_t power = 100; power >= 10; power-=10) {
		fan_power(power);
		osDelay(2000);
	}
	fan_disable();
}

/**
 * Perform sensor full power test
 */
void full_power_test() {
	osDelay(3000);
	sensor_MICS_power(POWER_ON);
    osDelay(3000);
    fan_power(100);
    osDelay(3000);
    sensor_MQ131_power(POWER_ON);
    osDelay(3000);
    sensor_MQ7_power(POWER_HIGH);
}

/**
 * MQ131 O3 Heater test (5 seconds)
 */
void heater_test_O3() {
	sensor_MQ131_power(POWER_ON);
	osDelay(5000);
	sensor_MQ131_power(POWER_OFF);
}

/**
 * MQ7 CO Heater test (5 seconds)
 */
void sensor_CO_heater_sequence() {
	sensor_MQ7_power(POWER_HIGH);
	osDelay(MQ_7_HI_TIME);
	sensor_MQ7_power(POWER_LOW);
	osDelay(MQ_7_LOW_TIME);
}

/**
 * Starts preheat timer for MQ7 sensor
 * @param hours
 */
void preheat_MQ7(uint8_t hours) {
	sensor_MQ7_power(POWER_HIGH);
	osTimerStart(preheatTimerHandle, hours * 3600000);
}

/**
 * Starts preheat timer for MQ131 sensor
 * @param hours
 */
void preheat_MQ131(uint8_t hours) {
	sensor_MQ131_power(POWER_HIGH);
	osTimerStart(preheatTimerHandle, hours * 3600000);
}


void sensor_power(atmo_sensor_t sensor, sensor_power_t power){
	switch (sensor) {
	case PQ_ATMO_MQ7:
		sensor_MQ7_power(power);
		break;
	case PQ_ATMO_MQ131:
		sensor_MQ131_power(power);
		break;
	case PQ_ATMO_MICS:
		sensor_MICS_power(power);
		break;
	default:
		break;
	}
}
/**
 * Set MQ-7 CO sensor power state
 * @param state
 */
void sensor_MQ7_power(sensor_power_t state) {
	switch (state) {
	case POWER_HIGH:
		set_pwm_duty_cycle(&htim1, TIM_CHANNEL_1, MQ_7_HI_PWM);
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
		break;
	case POWER_LOW:
		set_pwm_duty_cycle(&htim1, TIM_CHANNEL_1, MQ_7_LOW_PWM);
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
		break;
	case POWER_OFF:
		HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
		break;
	default:
		break;
	}
}

/**
 * Set MQ-131 O3 sensor power state
 * @param state
 */
void sensor_MQ131_power(sensor_power_t state) {
	switch (state) {
	case POWER_ON:
	case POWER_LOW:
	case POWER_HIGH:
		HAL_GPIO_WritePin(MQ131_V_GPIO_Port, MQ131_V_Pin, GPIO_PIN_RESET);
		break;
	case POWER_OFF:
		HAL_GPIO_WritePin(MQ131_V_GPIO_Port, MQ131_V_Pin, GPIO_PIN_SET);

		break;
	default:
		break;
	}
}

/**
 * Set MICS sensor power state
 * @param state
 */
void sensor_MICS_power(sensor_power_t state) {
	switch (state) {
	case POWER_ON:
	case POWER_LOW:
	case POWER_HIGH:
		HAL_GPIO_WritePin(MICS_V_GPIO_Port, MICS_V_Pin, GPIO_PIN_RESET);
		break;
	case POWER_OFF:
		HAL_GPIO_WritePin(MICS_V_GPIO_Port, MICS_V_Pin, GPIO_PIN_SET);

		break;
	default:
		break;
	}
}

/**
 * Get CO sensor voltage
 * @param adc
 * @return
 */
uint16_t sensor_read_CO(ADC_HandleTypeDef *adc) {
	uint16_t value;
	//TODO: Sensor activate sequence
	value = sensor_get_raw(adc, ADC_CHANNEL_MQ_7);
	//TODO: convert to PPM
	return value * SENSOR_CO_CALC;
}

/**
 * Get O3 sensor voltage
 * @param adc
 * @return milivolts
 */
uint16_t sensor_read_O3(ADC_HandleTypeDef *adc) {
	uint16_t value;
	//TODO: Sensor activate sequence
	value = sensor_get_raw(adc, ADC_CHANNEL_MQ_131);
	//TODO: convert to PPM
	return value * SENSOR_O3_CALC;
}

/**
 * Get O3 sensor voltage
 * @param adc
 * @return milivolts
 */
uint16_t sensor_read_MICS(ADC_HandleTypeDef *adc) {
	uint16_t value;
	//TODO: Sensor activate sequence
	value = sensor_get_raw(adc, ADC_CHANNEL_MICS);
	//TODO: convert to PPM
	return value * SENSOR_MICS_CALC;
}
