#include "bme280_wrap.h"

struct bme280_dev bme280;

int8_t bme280_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data,
		uint16_t len) {
	HAL_StatusTypeDef status = HAL_OK;
	if (HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t) (dev_id << 1), 3, 1000)
			!= HAL_OK) {
		return 1;
	}

	status = HAL_I2C_Mem_Read(&hi2c1, (uint8_t) (dev_id << 1),
			(uint8_t) reg_addr, I2C_MEMADD_SIZE_8BIT, reg_data, len, 1000);

	if (status != HAL_OK) {
		return 1;
	}
	return 0;
}

int8_t bme280_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data,
		uint16_t len) {
	HAL_StatusTypeDef status = HAL_OK;

	if (HAL_I2C_IsDeviceReady(&hi2c1, (dev_id << 1), 3, 1000) != HAL_OK) {
		return 1;
	}

	status = HAL_I2C_Mem_Write(&hi2c1, (dev_id << 1), reg_addr,
			I2C_MEMADD_SIZE_8BIT, reg_data, len, 1000);
//	TODO: Find a fix to remove this delay
	HAL_Delay(40);
	if (status != HAL_OK) {
		return 1;
	}
	return 0;
}

void bme280_delay_ms(uint32_t millisec) {
	HAL_Delay(millisec);
}

int8_t bme280_atmo_init() {
	int8_t rslt = BME280_OK;

	bme280.dev_id = BME280_I2C_ADDR_PRIM;
	bme280.intf = BME280_I2C_INTF;
	bme280.read = bme280_i2c_read;
	bme280.write = bme280_i2c_write;
	bme280.delay_ms = bme280_delay_ms;

	rslt = bme280_init(&bme280);
	return rslt;
}

int8_t bme280_read_sensor_data_normal_mode(struct bme280_data *comp_data) {
	int8_t rslt;
	uint8_t settings_sel;
//	struct bme280_data comp_data;

	/* Recommended mode of operation: Indoor navigation */
	bme280.settings.osr_h = BME280_OVERSAMPLING_1X;
	bme280.settings.osr_p = BME280_OVERSAMPLING_16X;
	bme280.settings.osr_t = BME280_OVERSAMPLING_2X;
	bme280.settings.filter = BME280_FILTER_COEFF_16;
	bme280.settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

	settings_sel = BME280_OSR_PRESS_SEL;
	settings_sel |= BME280_OSR_TEMP_SEL;
	settings_sel |= BME280_OSR_HUM_SEL;
	settings_sel |= BME280_STANDBY_SEL;
	settings_sel |= BME280_FILTER_SEL;
	rslt = bme280_set_sensor_settings(settings_sel, &bme280);
	rslt = bme280_set_sensor_mode(BME280_NORMAL_MODE, &bme280);
	rslt = bme280_get_sensor_data(BME280_ALL, comp_data, &bme280);
	return rslt;
}
